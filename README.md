# Bot Discord - Jeu de l'oie

## Utilisation du bot
#### Un bot sera bientôt développé afin de jouer directement sur votre serveur.

## Utilisation du code

### Dépendances nécessaires
Vous devez [installer node.js](https://nodejs.org/fr/download) sur votre ordinateur ainsi qu'[avoir une base de donnée MongoDB](https://www.youtube.com/watch?v=tbsM7hb1UUc) afin d'utiliser ce code.

### Cloner le code
- Pour cloner le code depuis git vous devez impérativement [installer git](https://git-scm.com/downloads) sur votre ordinateur.
- Une fois git installé, ouvrez `git bash` et tapez la commande suivante :
```
git clone https://framagit.org/Akerck/bot-discord-jeu-de-l-oie.git
```
- Ouvrez le dossier via un éditeur de code comme Visual Studio Code par exemple.
- Créez un fichier nommé `.env` à la racine et insérez le code suivant :
```
DISCORD_TOKEN = 'TOKEN DE VOTRE BOT'
DATABASE_URI = 'LIEN VERS VOTRE BASE DE DONNEES MONGODB'
```
- Ouvrez le terminal à l'emplacement de votre projet et tapez la commande suivante :
```
npm install
```
- Vous pouvez dès à présent lancer le code en faisant la commande suivante dans votre terminal à l'emplacement de votre projet :
```
node index.js
```

## Configuration

### Configuration du plateau
Vous trouverez la configuration du plateau de jeu à l'emplacement suivant : `utils/tools/config.json`. Chaque case est représentée par le code suivant :
```
{
  "case_index": 0,
  "case_number": 0,
  "case_action": false,
  "case_description": false,
  "case_image": false
}
```
- `"case_index"` : représente la case sur laquelle tombe le joueur.
- `"case_number"` : représente la case sur laquelle le joueur est redirigé. Si `"case_index"` est équivalent à `"case_number"` alors aucune redirection est faite.
- `"case_action"` : permet de préciser l'action effectuée sur la case ("Avancez à la case X", "Passez X tours", "Faites X lors de votre prochain lancé pour avancer", etc).
- `"case_description"` : permet d'ajouter une description à la case.
- `"case_image"` : permet d'ajouter une image à la case. Accepte les formats `png`, `jpeg` et `gif`

#### Attention vous ne pouvez pas ajouter plus de cases qu'il n'en existe déjà !

### Les pièges
Il existe 5 types de pièges :
- La redirection : lorsque `"case_index"` et `"case_number"` sont différents cela permet d'envoyer le joueur à une autre case lorsqu'il passe dessus.

Exemple : le joueur avance à la case 16 lorsqu'il tombe sur la case 8.
```
"case_index": 8,
"case_number": 16
```
- Le saut de tour : augmente le temps avant de pouvoir relancer lé dé pour le joueur.

Exemples : si `"case_malus_bonus"` est à 2, le joueur passe un tour ;
```
"case_malus_bonus_type": "cooldown",
"case_malus_bonus": 2
```
si `"case_malus_bonus"` est à 0, le joueur peut rejouer directement.
```
"case_malus_bonus_type": "cooldown",
"case_malus_bonus": 0
```
- Le bonus/malus permanent : ajoute un bonus ou un malus au dé du joueur.

Exemples : le joueur aura -1 à son dé lors des prochains lancés ;
```
"case_malus_bonus_type": "karma",
"case_malus_bonus": -1
```
Les bonus/malus sont réinitialisés.
```
"case_malus_bonus_type": "karma",
"case_malus_bonus": 0
```
#### Les bonus/malus ne sont pas cumulables !
- Le pierre-feuille-ciseaux : lance un pierre-feuille-ciseaux contre le joueur.

Exemple : si le joueur gagne, il avance de 3 cases ; si le joueur perd, il recule de 3 cases ; en cas d'égalité, le joueur ne bouge pas.
```
"case_malus_bonus_type": "chifoumi",
"case_malus_bonus": 3
```


## Contributeur.ice.s
- Développeur : Akerck
- Construction du plateau : L'eden
- Bêta Testers : Astri, I_am_TheSheep, lamatt2, Xav