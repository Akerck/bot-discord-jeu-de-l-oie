const { Client, Collection } = require('discord.js');
const dotenv = require('dotenv'); dotenv.config();
const mongoose = require('mongoose');
const client = new Client({ intents: 131071 });

['commands', 'ints', 'cooldown'].forEach(x => client[x] = new Collection());
['CommandUtil', 'EventUtil', 'InteractionUtil'].forEach(handler => { require(`./utils/handlers/${handler}`)(client) });
require('./utils/tools/functions')(client);

//EMPECHER LE BOT DE S'ARRETER => LE BOT RENVOIE LES ERREURS DANS LA CONSOLE
process.on('exit', code => { console.log(`Le processus c'est arrêté avec le code : ${code} !`) });
process.on('uncaughtException', (err, origin) => { console.log(`UNCAUGHT_EXCEPTION : ${err}`, `Origine : ${origin}`) });
process.on('unhandledRejection', (reason, promise) => { console.log(`UNHANDLE_REJECTION : ${reason}\n----------\n`, promise) });
process.on('warning', (...args) => { console.log(...args) });

mongoose.set('strictQuery', true)
mongoose.connect(process.env.DATABASE_URI, {
    autoIndex: false,
    maxPoolSize: 10,
    serverSelectionTimeoutMS: 5000,
    socketTimeoutMS: 45000,
    family: 4
}).then(() => console.log('Le client est connecté à la base de données !'))
    .catch(err => console.log(err));

client.login(process.env.DISCORD_TOKEN);