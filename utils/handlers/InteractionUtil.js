const { promisify } = require('util');
const { glob } = require('glob');
const pGlob = promisify(glob);
var path = require('path');

module.exports = async client => {
    (await pGlob(path.join(__dirname, '../../interactions/*/*.js'))).map(async intFile => {
        const int = require(intFile);

        if (!int.name) return console.log(`----------\nBoutton / menu déroulant non déclanché : pas de nom\nFichier -> ${intFile}\n----------`);

        client.ints.set(int.name, int);

    });
};