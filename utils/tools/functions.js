const { EmbedBuilder, ActionRowBuilder, ButtonBuilder, TextInputBuilder, ButtonStyle } = require("discord.js");

module.exports = async client => {

    //Use a wait function
    client.wait = async ms => {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    //Create easily an embed
    client.sembed = (msg, color, value) => {
        if (!color) color = '2f3136';
        if (!value) {
            return new EmbedBuilder().setColor(color).setDescription(msg);
        } else {
            return new EmbedBuilder().setColor(color).addFields([{ name: value, value: msg }]);
        }
    }

    //Create an embed error
    client.error = (msg) => {
        return new EmbedBuilder().setColor('#ff0000').setTitle('Une erreur est survenue :').setDescription(msg)
    }

    //Create easily a button
    client.sbutton = (msg, id, style, emote, disabled, row, link) => {
        if (!row) {
            if (!link) {
                const button = new ButtonBuilder();
                msg ? button.setLabel(msg) : undefined;
                style ? button.setStyle(style) : button.setStyle(ButtonStyle.Primary); //ButtonStyle.Primary / ButtonStyle.Secondary / ButtonStyle.Success / ButtonStyle.Danger / ButtonStyle.Link
                id ? button.setCustomId(id) : undefined;
                emote ? button.setEmoji(emote) : undefined;
                disabled ? button.setDisabled(disabled) : undefined;
                return new ActionRowBuilder().addComponents(button);
            } else {
                const button = new ButtonBuilder();
                msg ? button.setLabel(msg) : undefined;
                emote ? button.setEmoji(emote) : undefined;
                disabled ? button.setDisabled(disabled) : undefined;
                button.setStyle(ButtonStyle.Link);
                button.setURL(link);
                return new ActionRowBuilder().addComponents(button);
            }
        } else {
            if (!link) {
                const button = new ButtonBuilder();
                msg ? button.setLabel(msg) : undefined;
                style ? button.setStyle(style) : button.setStyle(ButtonStyle.Primary) //ButtonStyle.Primary / ButtonStyle.Secondary / ButtonStyle.Success / ButtonStyle.Danger / ButtonStyle.Link
                id ? button.setCustomId(id) : undefined;
                emote ? button.setEmoji(emote) : undefined;
                disabled ? button.setDisabled(disabled) : undefined;
                return row.addComponents(button);
            } else {
                const button = new ButtonBuilder();
                msg ? button.setLabel(msg) : undefined;
                emote ? button.setEmoji(emote) : undefined;
                disabled ? button.setDisabled(disabled) : undefined;
                button.setStyle(ButtonStyle.Link)
                button.setURL(link)
                return row.addComponents(button);
            }
        }
    }

    //Create easily a modal
    client.smodal = (title, id, style, disabled, maxLength, minLength, value, placeholder) => {
        const component = new TextInputBuilder();
        id ? component.setCustomId(id) : undefined;
        title ? component.setLabel(title) : undefined;
        style ? component.setStyle(style) : undefined; //TextInputStyle.Short = 1 / TextInputStyle.Paragraph = 2
        disabled ? component.setRequired(disabled) : undefined;
        maxLength ? component.setMaxLength(maxLength) : undefined;
        minLength ? component.setMinLength(minLength) : undefined;
        value ? component.setValue(value) : undefined;
        placeholder ? component.setPlaceholder(placeholder) : undefined;
        return component;
    }

    //Create easily a canva
    client.fitText = (ctx, text, x, y, maxWidth, maxHeight, font, fontsize, fontcolor, fontbold) => {
        let words = text.split(' ');
        let lines = [];
        let currentLine = '';

        fontbold ? fontbold = 'bold ' : fontbold = '';
        ctx.font = fontbold + fontsize + "px " + font;
        ctx.fillStyle = fontcolor;

        for (let i = 0; i < words.length; i++) {
            let testLine = currentLine + (currentLine ? ' ' : '') + words[i];
            let metrics = ctx.measureText(testLine);
            let lineWidth = metrics.width;

            if (lineWidth <= maxWidth) {
                currentLine = testLine;
            } else {
                lines.push(currentLine);
                currentLine = words[i];
            }
        }
        lines.push(currentLine);

        let totalHeight = lines.length * fontsize;
        if (totalHeight <= maxHeight) {
            ctx.textAlign = "center";
            for (let i = 0; i < lines.length; i++) {
                ctx.fillText(lines[i], x, y - totalHeight / 2 + (i + 1) * fontsize, maxWidth);
            }
        }
    }

    //Convert a Date to a String Date
    client.time = (time) => {
        const datePassee = new Date(time);
        const maintenant = Date.now();
        const differenceEnMillisecondes = maintenant - datePassee;

        // Conversion de la différence en millisecondes en jours, heures, minutes et secondes
        const jours = Math.floor(differenceEnMillisecondes / (1000 * 60 * 60 * 24));
        const heures = Math.floor((differenceEnMillisecondes % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        const minutes = Math.floor((differenceEnMillisecondes % (1000 * 60 * 60)) / (1000 * 60));
        const secondes = Math.floor((differenceEnMillisecondes % (1000 * 60)) / 1000);

        // Formatage du résultat
        return `${jours} jour${jours > 1 ? 's' : ''} ${heures} heure${heures > 1 ? 's' : ''} ${minutes} minute${minutes > 1 ? 's' : ''} ${secondes} seconde${secondes > 1 ? 's' : ''}`;
    }

    //Convert a String Date to a Object Date
    client.parseDuree = (texteDuree) => {
        const regex = /(\d+)\s+jour(?:s)?\s+(\d+)\s+heure(?:s)?\s+(\d+)\s+minute(?:s)?\s+(\d+)\s+seconde(?:s)?/i;
        const match = texteDuree.match(regex);

        if (match) {
            const jours = parseInt(match[1], 10);
            const heures = parseInt(match[2], 10);
            const minutes = parseInt(match[3], 10);
            const secondes = parseInt(match[4], 10);

            return {
                jours: jours,
                heures: heures,
                minutes: minutes,
                secondes: secondes
            };
        }

        return null; // Retourne null si le texte n'a pas le format attendu
    }

    //Compare two Object Dates
    function comparerDurees(duree1, duree2) {
        const heures1 = duree1.heures || 0;
        const minutes1 = duree1.minutes || 0;
        const secondes1 = duree1.secondes || 0;

        const heures2 = duree2.heures || 0;
        const minutes2 = duree2.minutes || 0;
        const secondes2 = duree2.secondes || 0;

        const duree1EnMillisecondes = ((heures1 * 60 + minutes1) * 60 + secondes1) * 1000;
        const duree2EnMillisecondes = ((heures2 * 60 + minutes2) * 60 + secondes2) * 1000;

        if (duree1EnMillisecondes >= duree2EnMillisecondes) {
            return true;
        } else if (duree1EnMillisecondes < duree2EnMillisecondes) {
            return false;
        }
    }

    //Convert String Date into seconds
    client.dureeEnSecondes = (duree) => {
        const regex = /(\d+) jours? (\d+) heures? (\d+) minutes? (\d+) secondes?/;
        const matches = duree.match(regex);
        if (!matches) return 0;

        const jours = parseInt(matches[1]) || 0;
        const heures = parseInt(matches[2]) || 0;
        const minutes = parseInt(matches[3]) || 0;
        const secondes = parseInt(matches[4]) || 0;

        return jours * 24 * 60 * 60 + heures * 60 * 60 + minutes * 60 + secondes;
    }

    //Create a leaderboard from String Date
    client.classementMeilleurTemps = (durees) => {
        // Créer une copie du tableau original pour éviter de modifier les durées d'origine
        const dureesCopie = durees.slice();

        // Convertir les durées en secondes et les trier
        dureesCopie.sort((a, b) => client.dureeEnSecondes(a) - client.dureeEnSecondes(b));

        return dureesCopie;
    }
}