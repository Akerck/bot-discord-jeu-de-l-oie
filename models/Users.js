const mongoose = require('mongoose');

const usersSchema = mongoose.Schema({
  userId: String,
  jdo: {
    case: {
      type: Number,
      default: 0
    },
    karma: {
      type: Number,
      default: 0
    },
    time: {
      type: Date,
      default: Date.now()
    },
    best_time: Date,
    mooves: {
      type: Number,
      default: 0
    },
    best_mooves: Number,
  }
});

module.exports = mongoose.model('Users', usersSchema);