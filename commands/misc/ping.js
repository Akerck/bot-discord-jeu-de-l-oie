//COMMANDE DESCRIPTION : A ping command to get the latence and the uptime of the bot

const { EmbedBuilder, PermissionsBitField } = require('discord.js');

module.exports = {
    name: 'ping',
    permissions: [PermissionsBitField.Flags.SendMessages],
    description: 'Commande ping !',
    runSlash(client, interaction) {

        const embed = new EmbedBuilder()
            .setTitle('🏓 Pong !')
            .setColor('2f3136')
            .setThumbnail(client.user.displayAvatarURL())
            .addFields(
                { name: 'Latence', value: `\`${client.ws.ping}ms\``, inline: true },
                { name: 'Uptime', value: `<t:${parseInt(client.readyTimestamp / 1000)}:R>`, inline: true }
            )
            .setTimestamp()
            .setFooter({ text: interaction.user.username, iconURL: interaction.user.displayAvatarURL() });

        interaction.reply({ embeds: [embed] });

    }
}