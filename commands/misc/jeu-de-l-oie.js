const { ButtonStyle, EmbedBuilder, ApplicationCommandOptionType } = require('discord.js');
const jdo = require('../../../CMFrBot_files/utils/storage/jdo.json');
const { collec_cooldown_jdl } = require('../../../CMFrBot_files/utils/tools/stockage');
const { Users } = require('../../../CMFrBot_files/utils/models/index');
// const { createCanvas, loadImage } = require('canvas');

module.exports = {
  name: 'jeu-de-l-oie',
  description: 'Jouer au jeu de l\'oie !',
  options: [
    {
      name: 'profil',
      description: 'Voir son profil',
      type: ApplicationCommandOptionType.Subcommand
    },
    {
      name: 'classement',
      description: 'Voir le classement des meilleurs temps',
      type: ApplicationCommandOptionType.Subcommand,
      options: [
        {
          name: 'type', description: 'Classement par meilleurs temps / meilleurs nombre de coups', type: ApplicationCommandOptionType.String, required: true,
          choices: [
            { name: 'temps', value: 'time' },
            { name: 'coups', value: 'mooves' }
          ]
        }
      ]
    },
    {
      name: 'dé',
      description: 'Lancer le dé !',
      type: ApplicationCommandOptionType.Subcommand,
      // options: [
      //   { name: 'case', description: 'Choisir la case sur laquelle se rendre', type: ApplicationCommandOptionType.Number, required: false }
      // ],
    },
    {
      name: 'reset',
      description: 'Réinitialiser votre partie',
      type: ApplicationCommandOptionType.Subcommand
    }
  ],
  async runSlash(client, interaction) {

    const admin_case = interaction.options.getNumber('case');
    const leaderboard_type = interaction.options.getString('type');
    const dbUser = await Users.findOne({ userId: interaction.user.id });
    if (!dbUser) {
      await Users.create({ userId: interaction.user.id }, err => {
        if (err) return console.log(err);
      });
      return interaction.reply({ embeds: [client.sembed('<:yes:821739920170287114> Le bot vient de créer votre profil car vous n\'en aviez pas précédemment ! Vous pouvez refaire votre commande.', '#2ECC71')] });
    }

    function parseDuration(durationMs) {

      // Conversion en unités de temps
      const seconds = Math.floor(durationMs / 1000) % 60;
      const minutes = Math.floor(durationMs / (1000 * 60)) % 60;
      const hours = Math.floor(durationMs / (1000 * 60 * 60)) % 24;
      const days = Math.floor(durationMs / (1000 * 60 * 60 * 24));

      // Construction de la chaîne de texte
      let result = '';
      if (days > 0) result += `${days} jour${days > 1 ? 's' : ''} `;
      if (hours > 0) result += `${hours} heure${hours > 1 ? 's' : ''} `;
      if (minutes > 0) result += `${minutes} minute${minutes > 1 ? 's' : ''} `;
      if (seconds > 0) result += `${seconds} seconde${seconds > 1 ? 's' : ''}`;

      return result.trim();
    }

    if (interaction.options.getSubcommand() === 'profil') {

      const embed = new EmbedBuilder()
        .setColor('#2ECC71')
        .setImage("https://cdn.discordapp.com/attachments/764962975567183922/1084548719338336418/Banniere.jpg")
        .setThumbnail(interaction.user.displayAvatarURL())
        .setDescription(`# 🦢 \`Vos statistiques !\`
        ➡️ Votre case actuelle : \`${dbUser.jdo.case}\`
        ✨ Votre karma actuel : \`${dbUser.jdo.karma}\`
        🕙 Votre temps actuel : ${dbUser.jdo.case === 0 ? `\`Aucun\`` : `\`${parseDuration(new Date() - dbUser.jdo.time)}\``}
        🎲 Votre nombre de coups actuel : ${dbUser.jdo.mooves === 0 ? `\`Aucun\`` : `\`${dbUser.jdo.mooves}\``}

        🏆 Votre meilleur temps : \`${dbUser.jdo.best_time ? parseDuration(dbUser.jdo.best_time) : 'Aucun'}\`
        🏆 Votre meilleur nombre de coups : \`${dbUser.jdo.best_mooves ? dbUser.jdo.best_mooves : 'Aucun'}\``)
        .setTimestamp()
        .setFooter({ text: interaction.user.username, iconURL: interaction.user.displayAvatarURL() });

      interaction.reply({ embeds: [embed], ephemeral: true });

    } else if (interaction.options.getSubcommand() === 'classement') {
      
      if (leaderboard_type === 'time') { // Classement en fonction du temps
        var sortingUsers = await Users.find({ "jdo.best_time": { $exists: true } }).sort({ "jdo.best_time": 1 }).limit(10);
        var descr = `# 🦢 \`Classement des meilleurs temps !\`\n> *Votre record personnel ➜* \`${dbUser.jdo.best_time ? parseDuration(dbUser.jdo.best_time) : 'Aucun'}\`\n\n${sortingUsers.length === 0 ? '`Aucun classement disponible`' : `${sortingUsers.map((e, i) => `## - \`${i + 1}#\` <@${e.userId}> ➜ \`${parseDuration(e.jdo.best_time)}\`\n`).join(' ')}`}`;
      } else if (leaderboard_type === 'mooves') { // Classement en fonction du nombre de coups
        var sortingUsers = await Users.find({ "jdo.best_mooves": { $exists: true } }).sort({ "jdo.best_mooves": 1 }).limit(10);
        var descr = `# 🦢 \`Classement des meilleurs nombre de coups !\`\n> *Votre record personnel ➜* \`${dbUser.jdo.best_mooves ? `\`${dbUser.jdo.best_mooves} coups\`` : 'Aucun'}\`\n\n${sortingUsers.length === 0 ? '`Aucun classement disponible`' : `${sortingUsers.map((e, i) => `## - \`${i + 1}#\` <@${e.userId}> ➜ \`${dbUser.jdo.best_mooves} coups\`\n`).join(' ')}`}`;
      }

      const embed = new EmbedBuilder()
        .setColor('#2ECC71')
        .setDescription(descr)
        .setTimestamp()
        .setImage("https://cdn.discordapp.com/attachments/764962975567183922/1084548719338336418/Banniere.jpg");

      interaction.reply({ embeds: [embed] });

    } else if (interaction.options.getSubcommand() === 'dé') {

      // Erreur en cas de cooldown
      // if (collec_cooldown_jdl.get(interaction.user.id) > Date.now()) return interaction.reply({ embeds: [client.error(`Vous êtes encore dans le cooldown !\nVous pourrez relancer le dé dans <t:${new Date(collec_cooldown_jdl.get(interaction.user.id) / 1000).getTime()}:R>`)], ephemeral: true });

      interaction.deferReply();

      // Définition des différentes variables (cooldown, dé, bonus/malus, etc)
      let cooldown = 7 * 60 * 1000;
      let replied = { embed: client.sembed('Une erreur inconnue est survenue.\nVeuillez contacter un administrateur du bot.') };
      const de = (Math.floor(Math.random() * 6) + 1);
      let bonus_malus_de = de + dbUser.jdo.karma
      let new_case = admin_case || dbUser.jdo.case + bonus_malus_de;
      // Si le dé affiche un nombre négatif à cause du malus alors dé = 0
      if (bonus_malus_de < 0) bonus_malus_de = 0;
      // Si le joueur fait un score supérieur à 63 alors retour en arrière sur le plateau
      if (new_case > 61) {
        bonus_malus_de = 62 - (new_case);
        new_case = dbUser.jdo.case + bonus_malus_de;
      }
      dbUser.jdo.mooves += 1;

      const embed = new EmbedBuilder()
        .setTitle('🦢 Jeu de l\'oie !')
        .setColor('#2ECC71')
        .setDescription(`Résultat du dé : **${de}** ${dbUser.jdo.karma < 0 ? `*(${dbUser.jdo.karma} malus)*` : `*(+${dbUser.jdo.karma} bonus)*`}\n\nVotre case : \`${dbUser.jdo.case}\` -> \`${new_case}\` ${(new_case) !== jdo[new_case].case_number ? `-> \`${jdo[new_case].case_number}\`` : ''}\n**Action :** ${jdo[new_case].case_description ? jdo[new_case].case_description : `Vous tombez sur la case ${jdo[new_case].case_index}.`}${jdo[new_case].case_action ? `\n**${jdo[new_case].case_action}**` : ''}`)
        .setTimestamp()
        .setFooter({ text: interaction.user.username, iconURL: interaction.user.displayAvatarURL() });

      // Si la case contient d'ores et déjà une image dans le fichier de config alors l'insérer
      if (jdo[new_case].case_image) {
        replied = { embeds: [embed.setImage(jdo[new_case].case_image)] };
        // Si aucune image n'existe dans le fichier de config 
      } else {

        var canvas = createCanvas(1080, 1080);
        ctx = canvas.getContext('2d');
        var background = await loadImage('./utils/images/background.png');
        ctx.drawImage(background, 0, 0, 1080, 1080);

        function fitText(ctx, text, x, y, maxWidth, maxHeight, font, fontsize, fontcolor, fontbold) {
          let words = text.split(' ');
          let lines = [];
          let currentLine = '';

          fontbold ? fontbold = 'bold ' : fontbold = '';
          ctx.font = fontbold + fontsize + "px " + font;
          ctx.fillStyle = fontcolor;

          for (let i = 0; i < words.length; i++) {
            let testLine = currentLine + (currentLine ? ' ' : '') + words[i];
            let metrics = ctx.measureText(testLine);
            let lineWidth = metrics.width;

            if (lineWidth <= maxWidth) {
              currentLine = testLine;
            } else {
              lines.push(currentLine);
              currentLine = words[i];
            }
          }
          lines.push(currentLine);

          let totalHeight = lines.length * fontsize;
          if (totalHeight <= maxHeight) {
            ctx.textAlign = "center";
            for (let i = 0; i < lines.length; i++) {
              ctx.fillText(lines[i], x, y - totalHeight / 2 + (i + 1) * fontsize, maxWidth);
            }
          }
        }

        fitText(ctx, jdo[new_case].case_index.toString(), canvas.width / 2, canvas.height * 4 / 10, canvas.width - 50, canvas.height, "Sans", 400, "white", true);

        fitText(ctx, jdo[new_case].case_action ? jdo[new_case].case_action : 'Aucune action', canvas.width / 2, canvas.height * 7 / 10, canvas.width - 50, canvas.height, "Eras ITC", 80, "white", false);

        replied = { files: [{ attachment: canvas.toBuffer(), name: 'case_jeu_de_l_oie.png' }], embeds: [embed.setImage('attachment://case_jeu_de_l_oie.png')] };

      }

      // Attribuer les différents types de malus / bonus
      if (jdo[new_case].case_malus_bonus_type === "cooldown") { // Malus "passe X tours"
        cooldown = cooldown * jdo[new_case].case_malus_bonus;
      } else if (jdo[dbUser.jdo.case].case_malus_bonus_type === "piege") { // Malus "faire X pour continuer à avancer"
        if (!jdo[dbUser.jdo.case].case_malus_bonus.includes(de) && new_case !== dbUser.jdo.case || bonus_malus_de === 0) {

          collec_cooldown_jdl.set(interaction.user.id, Date.now() + cooldown / 2);
          await client.wait(2000);
          return await interaction.editReply({ embeds: [client.error(`Résulat de votre dé : \`${de}\`.\nVous n'avez pas fait le score attendu (${jdo[dbUser.jdo.case].case_malus_bonus.join(' ou ')}) pour quitter votre case.`)] });

        }
      } else if (jdo[new_case].case_malus_bonus_type === "karma") { //Malus / Bonus "obtenir X à votre dé de manière permanente"
        dbUser.jdo.karma = jdo[new_case].case_malus_bonus;
      } else if (jdo[new_case].case_malus_bonus_type === "chifoumi") { //Malus / Bonus "gagner au pierre feuille ciseaux"
        const choix = ['🍃', '✂️', '🪨'];
        const rdmNb = Math.floor(Math.random() * choix.length);
        const rdmChoix = choix[rdmNb];

        await client.wait(2000);
        await interaction.editReply({ embeds: [embed], fetchReply: true }).then((msg) => {
          msg.react('🪨');
          msg.react('🍃');
          msg.react('✂️');

          // Create a reaction collector
          const filter = (reaction, user) => user.id === interaction.user.id;
          const collector = msg.createReactionCollector({ filter, max: 1 });
          collector.on('collect', async r => {
            if (rdmChoix === r.emoji.name) {
              embed.setDescription(`${r.emoji.name} VS ${rdmChoix}\n\n**Egalité.** Vous ne bougez pas de votre case.`);
            } else if (rdmChoix === '🪨' && r.emoji.name === '✂️' || rdmChoix === '🍃' && r.emoji.name === '🪨' || rdmChoix === '✂️' && r.emoji.name === '🍃') {
              embed.setDescription(`${r.emoji.name} VS ${rdmChoix}\n\n**Perdu...** Vous reculez de ${jdo[new_case].case_malus_bonus} cases.`);
              dbUser.jdo.case -= jdo[new_case].case_malus_bonus;
            } else {
              embed.setDescription(`${r.emoji.name} VS ${rdmChoix}\n\n**Gagné !** Vous avancez de ${jdo[new_case].case_malus_bonus} cases !`);
              dbUser.jdo.case += jdo[new_case].case_malus_bonus;
            }
            await interaction.editReply(replied);
            dbUser.save();
          });
        });
      }

      collec_cooldown_jdl.set(interaction.user.id, Date.now() + cooldown);

      if (new_case === 61) { // Si le joueur arrive à la case 61 alors...
        const new_score = new Date() - dbUser.jdo.time;
        if (!dbUser.jdo.best_time || new_score < dbUser.jdo.best_time) dbUser.jdo.best_time = new_score; // ...ajout du nouveau temps si celui-ci est meilleur que le précédent
        // ...réinitialisation de la case et du karma
        if (!dbUser.jdo.best_mooves || dbUser.jdo.mooves < dbUser.jdo.best_mooves) dbUser.jdo.best_mooves = dbUser.jdo.mooves; // ...ajout du nouveau nombre de coups si celui-ci est meilleur que le précédent
        // ...réinitialisation de la case et du karma
        dbUser.jdo.case = 0;
        dbUser.jdo.karma = 0;
        dbUser.jdo.mooves = 0;
        // Déclanchement d'un événement quête
        client.emit('quest', interaction.user, 'end_goosegame');
      } else {
        if (dbUser.jdo.case === 0) dbUser.jdo.time = Date.now();
        dbUser.jdo.case = jdo[new_case].case_number; // Si le joueur (re)lance une partie alors réinitialisation du temps
        if (dbUser.jdo.case === 0) { // Si le joueur voit son score reset alors réinitialisation du temps
          dbUser.jdo.time = Date.now();
          dbUser.jdo.karma = 0;
          dbUser.jdo.mooves = 0;
        }
      }
      dbUser.save();

      await client.wait(2000);
      await interaction.editReply(replied);

    } else if (interaction.options.getSubcommand() === 'reset') {

      interaction.reply({ embeds: [client.sembed('⚠️ Vous êtes entrain de réinitialiser votre partie. Appuyez sur le bouton si dessous pour confirmer.', 'Red')], ephemeral: true, components: [client.sbutton('Confirmer', 'button-jdo-confirmation', ButtonStyle.Danger, '⚠️')] });

    }
  }
}