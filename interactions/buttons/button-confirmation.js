const { Users } = require('../../models/index');

module.exports = {
  name: 'button-confirmation',
  async runInteraction(client, interaction) {

    const dbUser = await Users.findOne({ userId: interaction.user.id });

    if (!dbUser) interaction.reply({ embeds: [client.error('Vous n\'avez pas encore de profil !')], ephemeral: true });

    dbUser.jdo.case = 0;
    dbUser.jdo.karma = 0;
    dbUser.jdo.time = Date.now();
    dbUser.jdo.mooves = 0;
    dbUser.save();

    interaction.reply({ embeds: [client.sembed('✅ Votre profil viens d\'être réinitialiser.', 'Orange')], ephemeral: true });

  }
}