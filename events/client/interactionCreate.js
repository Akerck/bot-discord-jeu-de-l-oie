const { InteractionType, Collection } = require('discord.js');

module.exports = {
    name: 'interactionCreate',
    once: false,
    async execute(client, interaction) {

        if (interaction.type === InteractionType.ApplicationCommand) {

            const cmd = client.commands.get(interaction.commandName);
            if (!cmd) return interaction.reply({ embeds: [client.error('La commande ne semble pas exister.')] });

            //Test des permissions (Discord) nécessaires pour effectuer une commande
            if (!interaction.member.permissions.has(cmd.permissions)) return interaction.reply({ embeds: [client.error('Vous n\'avez pas les permissions suffisantes pour exécuter cette commande.')], ephemeral: true });

            //Création d'un cooldown d'une seconde entre chaque commande
            if (!client.cooldown.has(cmd.name)) client.cooldown.set(cmd.name, new Collection());
            const now = Date.now();
            const timeStamps = client.cooldown.get(cmd.name);
            const cooldownAmount = (cmd.cooldown || 3) * 1000;
            if (timeStamps.has(interaction.user.id)) {
                const expirationTime = timeStamps.get(interaction.user.id) + cooldownAmount;

                if (now < expirationTime) {
                    return interaction.reply({ embeds: [client.error(`Vous êtes dans le cooldown. Vous pourrez refaire cette commande <t:${new Date(expirationTime / 1000).getTime()}:R>`)], ephemeral: true });
                }
            }

            //Execution de la fonction runSlash qui lance l'intéraction
            const cmdOutup = cmd.runSlash(client, interaction);

            if (await cmdOutup !== false) {
                timeStamps.set(interaction.user.id, now);
                setTimeout(() => timeStamps.delete(interaction.user.id), cooldownAmount);
            }

        } else if (interaction.isButton() || interaction.isSelectMenu() || interaction.type === InteractionType.ModalSubmit) {

            const int = client.ints.get(interaction.customId);
            if (!int) return interaction.reply({ embeds: [client.error('Le bouton / menu déroulant ne semble pas exister.')] });
            int.runInteraction(client, interaction);

        }

    }
}