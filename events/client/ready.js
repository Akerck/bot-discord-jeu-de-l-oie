module.exports = {
    name: 'ready',
    once: true,
    async execute(client) {

        console.log(`Le bot est connecté sur ${client.guilds.cache.size} serveurs`);
        console.log("Servers:");
        client.guilds.cache.forEach(guild => console.log(` - ${guild.name}`));

        //Définition du statut du bot
        client.user.setPresence({ activities: [{ name: `Jouons au jeu de l'oie !` }], status: 'online' });

        //SLASH COMMANDE GLOBALE
        client.application.commands.set(client.commands.map(cmd => cmd));

    }
}