module.exports = {
  name: 'modal',
  once: false,
  async execute(client, interaction) {

    const int = client.ints.get(interaction.customId);
    if (!int) return interaction.reply({ embeds: [client.error('Le modal ne semble pas exister.')] });
    int.runInteraction(client, interaction);

  }
}